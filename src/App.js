import React, { Component } from 'react';
import logo from './fourelements.jpg';
import weather from './sun.png';
import './App.css';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      items: [],
      isLoaded: false,
    }
  }
  componentDidMount() {

    fetch('https://jsonplaceholder.typicode.com/users')
    .then(res => res.json())
    .then(json => {
        this.setState({
          isLoaded: true,
          items:json,
        })
    });


  }
  render() {

    var { isLoaded, items} = this.state;

    if (!isLoaded) {
      return <div>Loading...</div>
    }

    else {

      return (
          <div className="App">

              <ul>
                {items.map(item => (
                  <li key={item.id}>
                    Name: {item.name} | Email: {item.email} | City: {item.city}
                  </li>
                ))};
              </ul>
          </div>
      );
    }
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className = "App-title" >Welcome to the Weather app</h1>
         <p>
           Here is a 5 day forecast
          </p>
          <a
            className="App-link"
            href="https://weather.com/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Weather.com
          </a>
        </header>
        <div className="Days">
          <div class="Monday">
           <h1> Monday </h1>
           <img src= {weather} className = "App-weather" alt ="weather"/>
           <h5> The high is: 90° </h5>
           <h5> The low  is: 70° </h5>
           </div>
          <div class="Tuesday">
          <h1> Tuesday </h1>
          <img src= {weather} className = "App-weather" alt ="weather"/>
          <h5> The high is: 90° </h5>
          <h5> The low  is: 70° </h5>
           </div>
          <div class="Wednesday">
          <h1> Wednesday </h1>
          <img src= {weather} className = "App-weather" alt ="weather"/>
          <h5> The high is: 90° </h5>
          <h5> The low  is: 70° </h5>
          </div>
          <div class="Thursday">
          <h1> Thursday </h1>
          <img src= {weather} className = "App-weather" alt ="weather"/>
          <h5> The high is: 90° </h5>
          <h5> The low  is: 70° </h5>
          </div>
          <div class="Friday">
          <h1> Friday </h1>
          <img src= {weather} className = "App-weather" alt ="weather"/>
          <h5> The high is: 90° </h5>
          <h5> The low  is: 70° </h5>
          </div>
        </div>
      </div>
    );
  }
}
export default App;
